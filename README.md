Camel Groovy Sample
===================

This repo contains a sample Groovy script showing aggregation and throttling in Camel:
* Aggregation
	- Messages are aggregated by EntityId (a value within a node in the messages' XML)
	- The aggregation retains message for 10 seconds and up to 1,000 records
	- Just the latest (last on the queue) message with the same Id move forward -- others are discarded
* Throttling
	- In order not to overload the final endpoint, throttling is enabled and delivers messages at a rate of 10/second

How to run it
-------------

## Pre requisites
* Groovy (you might need to modify your grapeConfig.xml to correctly pull dependencies)
* A local ActiveMQ instance (with the default admin/admin account for the tests)
* Powershell 3.0+ for the manual tests

## Running the demo
1. Start the route:
```
> ./start-route.ps1
```
2. Enter the test folder and enqueue the test messages:
```
> cd testdata
> ./send-testdata.ps1
```
