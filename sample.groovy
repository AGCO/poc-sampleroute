import org.apache.camel.builder.RouteBuilder
import org.apache.camel.impl.DefaultCamelContext
import org.apache.camel.processor.aggregate.UseLatestAggregationStrategy

@Grab(group="org.apache.camel",module="camel-core",version="2.13.1")
@Grab(group="org.apache.camel",module="camel-groovy",version="2.13.1")
@Grab(group="org.apache.camel",module="camel-jms",version="2.13.1")
@Grab(group="org.apache.activemq",module="activemq-camel",version="5.6.0")
@Grab(group="org.apache.activemq",module="activemq-core",version="5.6.0")
@Grab(group="org.apache.activemq",module="activemq-pool",version="5.6.0")
@Grab(group="org.slf4j",module="slf4j-jdk14",version="1.7.5")
class SampleRouteBuilder extends RouteBuilder {
	int num = 0;
	def number = {++num};

	void configure() {
		from("activemq:SampleInputQueue")
		.aggregate(xpath("/InputSample/EntityId", String.class), new UseLatestAggregationStrategy())
			.completionSize(1000)
			.completionTimeout(10000)
		.throttle(10)
			.timePeriodMillis(1000)
		.to("activemq:SampleOutputQueue")
	}
}

def camelContext = new DefaultCamelContext()
camelContext.addRoutes(new SampleRouteBuilder())

camelContext.start()
System.console().readLine()
camelContext.stop()