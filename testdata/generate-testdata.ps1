function Generate-TestData($sampleName, $count = 10) {
	$bn = $sampleName -replace "(.*)\.xml", '$1'
	[xml] $s = Get-Content $sampleName
	for ($i = 1; $i -le $count; ++$i) { 
		[string]$t = Get-Date -Format o 
		$s.InputSample.Timestamp = $t 
		$s.InputSample.Operation = 'U'
		$p = Get-Location
		$n = "{1}_{0:D2}.xml" -f $i, $bn
		$f = Join-Path $p $n 
		$s.Save($f) 
	}
}

echo 'Usage: Generate-TestData "sample-data" "number-of-copies"'